package com.tlead.narithprak.androidsession;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tlead.narithprak.androidsession.common.Item;
import com.tlead.narithprak.androidsession.common.ItemAPI;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    Toolbar toolbarWidget;
    private TextView id;
    private TextView title;
    private TextView author;
    private TextView price;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        toolbarWidget = (Toolbar) findViewById(R.id.toolbar1);
        toolbarWidget.setTitle("Detail");
        setSupportActionBar(toolbarWidget);
        new ItemAPI().getDetail(8).enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if(response.isSuccessful()){
                    setupData(response.body());
                }else {
                    Toast.makeText(DetailActivity.this, "failed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                Toast.makeText(DetailActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupData(Item item) {
        id = findViewById(R.id.tfID);
        title = findViewById(R.id.tfTitle);
        author = findViewById(R.id.tfAuthor);
        price = findViewById(R.id.tfPrice);
        imageView = findViewById(R.id.imageView2);

        id.setText(item.getId());
        title.setText(item.getTitle());
        author.setText(item.getAuthor());
        price.setText(item.getPrice());
        Picasso.get().load(item.getImage()).placeholder(R.drawable.awww).into(imageView);
    }

}
