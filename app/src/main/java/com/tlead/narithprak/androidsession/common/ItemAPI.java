package com.tlead.narithprak.androidsession.common;

import android.support.annotation.NonNull;

import com.tlead.narithprak.androidsession.Login.AuthenticationInterceptor;

import java.util.List;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ItemAPI {

    private static final String ENDPOINT = "http://assignment.gae.golgek.mobi/";

    public Call<ResponseBody> login(@NonNull String userName, @NonNull String password) {
        OkHttpClient loginClient = getLoginClient(userName,password);
        Retrofit.Builder builder = getBuilder().client(loginClient);
        Retrofit retrofit = builder.build();
        IItemAPI itemAPI = retrofit.create(IItemAPI.class);
        return itemAPI.login();
    }

    public Call<Item> getDetail(int itemId) {
        Retrofit.Builder builder = getBuilder().client(getDefailtClient());
        Retrofit retrofit = builder.build();
        IItemAPI itemAPI = retrofit.create(IItemAPI.class);
        return itemAPI.getItemById(itemId);
    }

    private OkHttpClient getLoginClient(String userName, String password) {
        String basic = Credentials.basic(userName, password);
        AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor(basic);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(authenticationInterceptor);
//                .addInterceptor(loggingInterceptor);
        return builder.build();
    }

    public Call<List<Item>> getItemsClient() {
        Retrofit.Builder builder = getBuilder().client(getDefailtClient());
        Retrofit retrofit = builder.build();
        IItemAPI itemAPI = retrofit.create(IItemAPI.class);
        return itemAPI.getItems();
    }

    private OkHttpClient getDefailtClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor);
        return builder.build();
    }


    @NonNull
    private Retrofit.Builder getBuilder() {
        return new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create());
    }

}
