package com.tlead.narithprak.androidsession;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tlead.narithprak.androidsession.common.ItemAPI;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbarWidget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbarWidget = (Toolbar) findViewById(R.id.toolbar1);
        toolbarWidget.setTitle("Login");
        toolbarWidget.setNavigationIcon(R.drawable.ic_launcher);
        setSupportActionBar(toolbarWidget);
        Button login = findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserCredential();
            }
        });
        ImageView companyImage = findViewById(R.id.imageView);
        String imageUrl = "https://www.cesarsway.com/sites/newcesarsway/files/styles/large_article_preview/public/Natural-Dog-Law-2-To-dogs%2C-energy-is-everything.jpg?itok=Z-ujUOUr";
        Picasso.get().load(imageUrl).placeholder(R.drawable.awww).into(companyImage);
//        Glide.with(this).load(imageUrl).into(companyImage);
    }

    private void login() {
        new ItemAPI().login("usertest","secret").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(MainActivity.this, "Success"    , Toast.LENGTH_SHORT).show();
                startDetail();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "failed"    , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startDetail() {
        Intent intent = new Intent(this, RecylceViewActivity.class);
        startActivity(intent);
    }

    private void validateUserCredential() {
        EditText editTextUser = findViewById(R.id.tfUsername);
        EditText editTextpassword = findViewById(R.id.tfPassword);

        String userName = editTextUser.getText().toString();
        String password = editTextpassword.getText().toString();
        if (TextUtils.isEmpty(userName) && TextUtils.isEmpty(password)) {
            login();
        } else {
            Toast.makeText(MainActivity.this, "Username or password need to be valid!"    , Toast.LENGTH_LONG).show();
        }

    }
}
