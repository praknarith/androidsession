package com.tlead.narithprak.androidsession.common;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IItemAPI {
    @GET("/api/v1/secure/")
    Call<ResponseBody> login();

    @GET("/api/v1/items/{id}")
    Call<Item> getItemById(@Path("id") int itemId);

    @GET("/api/v1/items/")
    Call<List<Item>> getItems();
}
