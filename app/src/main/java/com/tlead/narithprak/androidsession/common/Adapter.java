package com.tlead.narithprak.androidsession.common;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tlead.narithprak.androidsession.R;
import java.util.List;

public class Adapter extends BaseAdapter {

    List<Item> items;

    public Adapter(List<Item> itemList) {
        items = itemList;
        return;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_view,parent,false);
        }

        TextView title = convertView.findViewById(R.id.txtTitle);
//        TextView author = convertView.findViewById(R.id.txtAuthor);
//        TextView price = convertView.findViewById(R.id.txtPrice);
        title.setText(item.getTitle());
//        author.setText("Narito");
//        price.setText(item.getPrice());

        return convertView;
    }
}
