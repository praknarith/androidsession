package com.tlead.narithprak.androidsession;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.tlead.narithprak.androidsession.common.Adapter;
import com.tlead.narithprak.androidsession.common.Item;
import com.tlead.narithprak.androidsession.common.ItemAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {

    Toolbar toolbarWidget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        toolbarWidget = (Toolbar) findViewById(R.id.toolbar1);
        toolbarWidget.setTitle("List");
        setSupportActionBar(toolbarWidget);

        final ListView listView = findViewById(R.id.list_item);
        new ItemAPI().getItemsClient().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if (response.isSuccessful()) {
                    listView.setAdapter(new Adapter(response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
//                Toast.makeText(this,"Failed",Toast.LENGTH_LONG).show();
            }
        });
//        listView.setAdapter(new Adapter());
    }
}
