package com.tlead.narithprak.androidsession;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.tlead.narithprak.androidsession.common.Item;
import com.tlead.narithprak.androidsession.common.ItemAPI;
import com.tlead.narithprak.androidsession.recycle.ItemAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecylceViewActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recylce_view);

        Toolbar toolbarWidget = (Toolbar) findViewById(R.id.toolbar1);
        toolbarWidget.setTitle("Login");
        toolbarWidget.setNavigationIcon(R.drawable.ic_launcher);
        setSupportActionBar(toolbarWidget);

        recyclerView = findViewById(R.id.my_recycler_view);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        new ItemAPI().getItemsClient().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if (response.isSuccessful()) {
                    adapter = new ItemAdapter(response.body());
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
//                Toast.makeText(this,"Failed",Toast.LENGTH_LONG).show();
            }
        });

    }
}
